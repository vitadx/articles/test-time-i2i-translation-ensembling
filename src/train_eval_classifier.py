import os
import json
import torch
import argparse
import numpy as np
from torch import nn
from torchvision import transforms
from torchvision import models
from torch.optim import Adam
from wilds import get_dataset
from torch.optim.lr_scheduler import LambdaLR
from torch.utils.data import DataLoader
import torch.multiprocessing

from datasets import ClassificationWildsDataset
from models.starganv2 import (
    StyleEncoder,
    MappingNetwork,
    Generator,
    StyleAugMix)
from utils.augmentations import RandomRotation90, TwoCropsTransform
from models.ema import EMA
from trainers.classifier import Trainer

pretrained_model = {
    model_name: getattr(models, model_name) for model_name in dir(models)}


def main(
        data_root_dir,
        i2i_chkpt_path,
        json_config,
        log_dir,
        checkpoint_dir,
        result_dir,
        exp_index):

    # Load config json file
    with open(json_config, "r") as f:
        hyperparameters = json.load(f)

    CHECKPOINT_DIR = os.path.join(
        checkpoint_dir, hyperparameters["DATASET_NAME"],
        hyperparameters["EXP_NAME"], f"exp_{exp_index}")
    hyperparameters.update({
        "ROOT_DIR": data_root_dir,
        "LOG_DIR": os.path.join(
            log_dir, hyperparameters["DATASET_NAME"],
            hyperparameters["EXP_NAME"], f"exp_{exp_index}"),
        "CHECKPOINT_PATH": os.path.join(
            CHECKPOINT_DIR, f"{hyperparameters['MODEL_NAME']}.pth"),
        "RESULT_DIR": os.path.join(
            result_dir, hyperparameters["DATASET_NAME"],
            hyperparameters["EXP_NAME"], f"exp_{exp_index}")
    })

    # First copy zip from workdir into node tmp dir and then unzip
    DEVICE = torch.device("cuda")

    # Get transformations
    train_transform = transforms.Compose([
        transforms.RandomResizedCrop(
            hyperparameters["RESIZED_SHAPE"], scale=(0.7, 1),
            interpolation=transforms.InterpolationMode.BILINEAR),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.RandomVerticalFlip(p=0.5),
        RandomRotation90(),
        transforms.ToTensor(),
        # transforms.RandomErasing(scale=(0.02, 0.2)),
        transforms.Normalize(
            (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    test_transform = transforms.Compose([
        transforms.Resize(hyperparameters["RESIZED_SHAPE"]),
        transforms.ToTensor(),
    ])
    train_two_crops_transform = TwoCropsTransform(
        train_transform, train_transform)

    # Get train, val, test datasets and dataloaders
    print("Loading dataset ...")
    splits = {}
    wilds_dataset = get_dataset(
        dataset=hyperparameters["DATASET_NAME"],
        root_dir=data_root_dir,
        download=True)
    n_classes = wilds_dataset.n_classes

    for split_name in wilds_dataset._split_names:
        transform = train_two_crops_transform if split_name == 'train' else test_transform
        splits[split_name] = ClassificationWildsDataset(
            wilds_dataset=wilds_dataset,
            split_name=split_name,
            domain_label_name=hyperparameters["DOMAIN_LABEL_NAME"],
            transform=transform)

    print("Creating dataloader...")
    dataloaders = {}
    for split_name, dataset in splits.items():
        is_train_dataset = split_name == 'train'
        batch_size = hyperparameters["BATCH_SIZE"] if is_train_dataset else int(
            hyperparameters["BATCH_SIZE"]//2)
        num_workers = 0
        dataloaders[split_name] = DataLoader(
            dataset=dataset,
            batch_size=batch_size,
            num_workers=num_workers,
            shuffle=is_train_dataset,
            drop_last=is_train_dataset)

    # Initialize networks and optimizers
    print("Creating classifier...")
    print(f"Using pretrained classifier: {hyperparameters['USE_PRETRAINING']}")
    model = pretrained_model[hyperparameters["MODEL_NAME"]](
        pretrained=hyperparameters["USE_PRETRAINING"])
    if "resnet" in hyperparameters["MODEL_NAME"]:
        model.fc = nn.Linear(
            model.fc.in_features, n_classes)
    elif "densenet" in hyperparameters["MODEL_NAME"]:
        model.classifier = nn.Linear(
            model.classifier.in_features, n_classes)
    model = model.to(DEVICE)

    networks = {}
    # Load image to image translation model
    i2i_chkpt = torch.load(
        i2i_chkpt_path)
    i2i_hyperparams = i2i_chkpt["hyperparameters"]
    print("Loading pretrained StarGanV2 model...")
    style_encoder = StyleEncoder(
        img_size=i2i_hyperparams["IMG_SIZE"],
        style_dim=i2i_hyperparams["STYLE_DIM"],
        num_domains=i2i_hyperparams["NUM_DOMAINS"]).to(DEVICE)
    mapping_network = MappingNetwork(
        latent_dim=i2i_hyperparams["LATENT_DIM"],
        style_dim=i2i_hyperparams["STYLE_DIM"],
        num_domains=i2i_hyperparams["NUM_DOMAINS"],
        hidden_dim=512).to(DEVICE)
    generator = Generator(
        img_size=i2i_hyperparams["IMG_SIZE"],
        style_dim=i2i_hyperparams["STYLE_DIM"],
        use_sn=False).to(DEVICE)
    style_encoder = EMA(
        style_encoder, decay=i2i_hyperparams["EMA_DECAY"]).eval()
    mapping_network = EMA(
        mapping_network, decay=i2i_hyperparams["EMA_DECAY"]).eval()
    generator = EMA(generator, decay=i2i_hyperparams["EMA_DECAY"]).eval()
    style_encoder.load_state_dict(i2i_chkpt["networks"]["S_EMA"])
    mapping_network.load_state_dict(i2i_chkpt["networks"]["M_EMA"])
    generator.load_state_dict(i2i_chkpt["networks"]["G_EMA"])
    style_encoder = style_encoder.ema_model
    mapping_network = mapping_network.ema_model
    generator = generator.ema_model
    networks["style_aug"] = StyleAugMix(
        generator=generator,
        mapping_network=mapping_network,
        style_encoder=style_encoder,
        classifier=model,
        num_styles=hyperparameters["NUM_STYLES"],
        device=DEVICE).to(DEVICE).eval()
    print(f'The number of domains is: {networks["style_aug"].mapping_network.num_domains}')

    networks["classifier"] = model
    optimizers = {
        "classifier": Adam(
            lr=hyperparameters["LR"], params=networks["classifier"].parameters())
    }

    schedulers = {
        "classifier": LambdaLR(
            optimizers["classifier"],
            lr_lambda=lambda epoch: 0.5 * (
                1 + np.cos(np.pi * (epoch / hyperparameters["EPOCHS"]))),
            verbose=False)
    }

    # Make sure that log_dir and checkpoint_dir exists and are created
    os.makedirs(CHECKPOINT_DIR, exist_ok=True)

    if os.path.exists(hyperparameters["CHECKPOINT_PATH"]):
        print("Loading state dict...")
        chkpt = torch.load(hyperparameters["CHECKPOINT_PATH"])
        for net_name, state_dict in chkpt["networks"].items():
            networks[net_name].load_state_dict(
                state_dict)
        for opt_name, state_dict in chkpt["optimizers"].items():
            optimizers[opt_name].load_state_dict(
                state_dict)
        for sched_name, state_dict in chkpt["schedulers"].items():
            schedulers[sched_name].load_state_dict(
                state_dict)

    # Train the model and evaluate
    trainer = Trainer()
    trainer.train(
        networks=networks,
        dataloaders=dataloaders,
        optimizers=optimizers,
        schedulers=schedulers,
        epochs=hyperparameters["EPOCHS"],
        steps_per_epoch=hyperparameters["STEPS_PER_EPOCH"],
        hyperparameters=hyperparameters,
        device=DEVICE,
        log_dir=hyperparameters["LOG_DIR"],
        checkpoint_path=hyperparameters["CHECKPOINT_PATH"],
        save_every_epochs=hyperparameters["SAVE_EVERY_EPOCHS"],
        eval_every_epochs=hyperparameters["EVAL_EVERY_EPOCHS"])

    # Evaluate the model with and without TTA
    networks["classifier"] = networks["classifier"].eval()
    metrics = {
        'without_tta': {},
        'with_tta': {}
    }
    for split_name, dataloader in dataloaders.items():
        if split_name != 'train':
            metrics['without_tta'][split_name] = trainer.evaluate_without_tta(
                classifier=networks['classifier'],
                dataloader=dataloader,
                hyperparameters=hyperparameters,
                device=DEVICE)
            metrics['with_tta'][split_name] = trainer.evaluate_with_tta(
                classifier=networks['classifier'],
                mapping_network=mapping_network,
                generator=generator,
                dataloader=dataloader,
                hyperparameters=hyperparameters,
                device=DEVICE)

    results = {
        "hyperparameters": hyperparameters,
        "metrics": metrics
    }
    os.makedirs(hyperparameters["RESULT_DIR"], exist_ok=True)
    RESULT_PATH = os.path.join(
        hyperparameters["RESULT_DIR"], f"{hyperparameters['MODEL_NAME']}.json")
    with open(RESULT_PATH, "w") as f:
        json.dump(results, f, indent=4)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data_root_dir",
        help="Path to data root directory.", type=str)
    parser.add_argument(
        "--i2i_chkpt_path",
        help="StarGanV2 checkpoint path.", type=str)
    parser.add_argument(
        "--json_config",
        help="Path to config json file", type=str)
    parser.add_argument("--log_dir", help="Logs directory", type=str)
    parser.add_argument(
        "--checkpoint_dir", help="Checkpoints directory", type=str)
    parser.add_argument(
        "--result_dir", help="Results directory", type=str)
    parser.add_argument(
        "--exp_index", help="Index of the experiment", type=str)

    args = parser.parse_args()
    main(
        data_root_dir=args.data_root_dir,
        i2i_chkpt_path=args.i2i_chkpt_path,
        json_config=args.json_config,
        log_dir=args.log_dir,
        checkpoint_dir=args.checkpoint_dir,
        result_dir=args.result_dir,
        exp_index=args.exp_index,
    )
