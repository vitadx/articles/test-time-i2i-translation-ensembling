import tqdm
import numpy as np

import torch
from torch.nn import functional as F
from torch.utils.tensorboard import SummaryWriter
from kornia import enhance
from sklearn.metrics import f1_score

from .metrics import AverageMeter
from trainers.losses import JSD


class Trainer():

    def __init__(self):
        pass

    def create_metrics(
            self):
        metrics = {
            dataset_name: {} for dataset_name in ["train", "val", "test"]}
        metrics["train"] = {
            "loss": AverageMeter(name="loss"),
            "ce_loss": AverageMeter(name="ce_loss"),
            "consistency_loss": AverageMeter(name="consistency_loss"),
        }
        return metrics

    def set_requires_grad(self, model, flag):
        for p in model.parameters():
            p.requires_grad = flag

    def train(
            self,
            networks,
            dataloaders,
            optimizers,
            schedulers,
            epochs,
            steps_per_epoch,
            hyperparameters,
            device,
            log_dir,
            checkpoint_path,
            save_every_epochs,
            eval_every_epochs):
        grad_scalers = {
            "classifier": torch.cuda.amp.GradScaler(
                enabled=hyperparameters["USE_MIXED_PRECISION"])}

        augmentations = {}
        criterions = {
            'jsd': JSD(temperature=0.5).to(device)
        }

        writer = SummaryWriter(log_dir)

        if steps_per_epoch is None:
            steps_per_epoch = len(dataloaders["train"])
        global_step = int(
            schedulers["classifier"].last_epoch * steps_per_epoch)

        for epoch in range(schedulers["classifier"].last_epoch, epochs):

            running_metrics = self.create_metrics()
            pbar = tqdm.tqdm(
                dataloaders["train"],
                total=steps_per_epoch,
                desc="Epoch: {}/{}".format(epoch+1, epochs))

            for batch_index, batch in enumerate(pbar, start=1):
                outputs, losses = self.train_on_batch(
                    networks,
                    batch,
                    optimizers,
                    grad_scalers,
                    hyperparameters,
                    augmentations,
                    criterions,
                    device,
                    global_step,
                )
                global_step += 1

                for metric_name in running_metrics["train"]:
                    running_metrics["train"][metric_name].update(
                        losses[metric_name].item(), 1)

                if batch_index == steps_per_epoch:
                    break

                desc = "Epoch: {}/{} -- {}".format(
                    epoch+1,
                    epochs,
                    ", ".join(
                        ["{}: {:.3f}".format(metric_name, metric.avg)
                         for metric_name, metric in running_metrics["train"].items()]))
                pbar.set_description(desc)

            # Write the learning rate, losses and acc
            writer.add_scalar(
                "train/lr", schedulers["classifier"].get_last_lr()[0],
                global_step=global_step)

            # Update the learning rate
            schedulers["classifier"].step()

            if (epoch+1) % eval_every_epochs == 0:

                print("Eval")
                networks["classifier"] = networks["classifier"].eval()
                for split_name, dataloader in dataloaders.items():
                    if split_name != 'train':
                        running_metrics[split_name] = self.evaluate_without_tta(
                            networks['classifier'],
                            dataloader,
                            hyperparameters,
                            device)
                networks["classifier"] = networks["classifier"].train()

            if (epoch+1) % save_every_epochs == 0:
                print("Dumping model")
                torch.save({
                    "networks": {
                        network_name: network.state_dict() for network_name,
                        network in networks.items()},
                    "optimizers": {
                        optimizer_name: optimizer.state_dict() for optimizer_name,
                        optimizer in optimizers.items()},
                    "schedulers": {
                        scheduler_name: scheduler.state_dict() for scheduler_name,
                        scheduler in schedulers.items()},
                    "hyperparameters": hyperparameters},
                    checkpoint_path)

            for split_name in running_metrics:
                for metric_name, metric in running_metrics[
                        split_name].items():
                    writer.add_scalar(
                        "{}/{}".format(split_name, metric_name),
                        metric.avg if isinstance(metric, AverageMeter) else metric,
                        global_step)

    def train_on_batch(
            self,
            networks,
            batch,
            optimizers,
            grad_scalers,
            hyperparameters,
            augmentations,
            criterions,
            device,
            global_step):
        with torch.cuda.amp.autocast(
                enabled=hyperparameters["USE_MIXED_PRECISION"]):
            x, d, y, *_ = batch
            x1, x2 = x
            x1, x2, d, y = x1.to(device), x2.to(device), d.to(device), y.to(device)
            # images should be in the range [-1, 1]
            x_adv_1, y_adv = networks["style_aug"](x1, d, y)
            x_adv_2, y_adv = networks["style_aug"](x2, d, y)

            # Convert images [-1; 1] --> [0; 1]
            optimizers["classifier"].zero_grad()
            x_adv_1 = (x_adv_1 + 1)/2
            x_adv_2 = (x_adv_2 + 1)/2

            if hyperparameters["USE_PRETRAINING"]:
                m = torch.tensor([0.485, 0.456, 0.406], device=device)
                s = torch.tensor([0.229, 0.224, 0.225], device=device)
                x_adv_1 = enhance.normalize(x_adv_1, mean=m, std=s)
                x_adv_2 = enhance.normalize(x_adv_2, mean=m, std=s)

            logits_x1 = networks["classifier"](x_adv_1)
            logits_x2 = networks["classifier"](x_adv_2)

            ce_loss = 1/2 * (
                F.cross_entropy(logits_x1, y) + F.cross_entropy(logits_x2, y))
            consistency_loss = hyperparameters['LAMBDA_CONSISTENCY'] * criterions['jsd'](
                logits_x1, logits_x2)
            loss = ce_loss + consistency_loss

        grad_scalers["classifier"].scale(loss).backward()
        grad_scalers["classifier"].step(optimizers["classifier"])
        grad_scalers["classifier"].update()

        outputs = {
            'x1': (x1+1)/2,
            'x2': (x2+1)/2,
            "x_adv_1": x_adv_1,
            "x_adv_2": x_adv_2,
        }
        metrics = {
            "loss": loss,
            "ce_loss": ce_loss,
            "consistency_loss": consistency_loss,
        }
        return outputs, metrics

    def evaluate_without_tta(
            self,
            classifier,
            dataloader,
            hyperparameters,
            device):
        y_true = []
        y_pred = []
        pbar = tqdm.tqdm(
            dataloader,
            total=len(dataloader),
            desc="Evaluation without TTA")
        with torch.no_grad():
            with torch.cuda.amp.autocast(
                    enabled=hyperparameters["USE_MIXED_PRECISION"]):
                for i, (X, _, target, *_) in enumerate(pbar):
                    X = X.to(device)
                    target = target.to(device)
                    if hyperparameters['USE_PRETRAINING']:
                        X = enhance.normalize(
                            X,
                            mean=torch.tensor(
                                [0.485, 0.456, 0.406], device=device),
                            std=torch.tensor(
                                [0.229, 0.224, 0.225], device=device))
                    pred = classifier(X).argmax(dim=-1)
                    y_true.append(target.cpu())
                    y_pred.append(pred.cpu())

        y_true = torch.cat(y_true).numpy()
        y_pred = torch.cat(y_pred).numpy()
        metrics = {
            "acc": np.mean(y_pred == y_true),
            "weighted_f1": f1_score(
                y_true=y_true, y_pred=y_pred, average='weighted')
        }
        return metrics

    def evaluate_with_tta(
            self,
            classifier,
            mapping_network,
            generator,
            dataloader,
            hyperparameters,
            device):
        y_true = []
        y_pred = []
        pbar = tqdm.tqdm(
            dataloader,
            total=len(dataloader),
            desc="Evaluation with TTA")
        with torch.no_grad():
            with torch.cuda.amp.autocast(
                    enabled=hyperparameters["USE_MIXED_PRECISION"]):
                for i, (X, _, target, *_) in enumerate(pbar):
                    X = X.to(device)
                    target = target.to(device)

                    # Convert images from [0, 1] to [-1, 1] for StarGanV2 usage
                    X = (X - 0.5)/0.5

                    # Project each image into every single source domain
                    num_domains = mapping_network.num_domains
                    latent_dim = mapping_network.latent_dim
                    # (N, C, H, W) --> (N * S, C, H, W)
                    n, c, h, w = X.shape
                    X = X.unsqueeze(1).repeat(
                        [1, num_domains, 1, 1, 1]).reshape([-1, c, h, w])
                    z = torch.randn(X.size(0), latent_dim, device=device)
                    d = torch.arange(num_domains, device=device).unsqueeze(
                        0).repeat([n, 1]).flatten()
                    s = mapping_network(z, d)
                    X_gen, _ = generator(X, s)

                    # Convert generated images from [-1, 1] to [0, 1] for classifier
                    X_gen = (X_gen+1)/2
                    if hyperparameters['USE_PRETRAINING']:
                        X_gen = enhance.normalize(
                            X_gen,
                            mean=torch.tensor(
                                [0.485, 0.456, 0.406], device=device),
                            std=torch.tensor(
                                [0.229, 0.224, 0.225], device=device))
                    predictions = classifier(X_gen)
                    predictions = predictions.reshape([n, num_domains, -1])
                    pred = predictions.mean(dim=1).argmax(dim=-1)
                    y_true.append(target.cpu())
                    y_pred.append(pred.cpu())

        y_true = torch.cat(y_true).numpy()
        y_pred = torch.cat(y_pred).numpy()
        metrics = {
            "acc": np.mean(y_pred == y_true),
            "weighted_f1": f1_score(
                y_true=y_true, y_pred=y_pred, average='weighted')
        }
        return metrics
