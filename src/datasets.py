import os
import random
import PIL
import numpy as np
from PIL import Image
from PIL import ImageFile
from torch.utils.data import Dataset

ImageFile.LOAD_TRUNCATED_IMAGES = True
PIL.Image.MAX_IMAGE_PIXELS = 888150000


def get_path(dataset_name, data_dir, filename):
    img_path = os.path.join(
        data_dir, filename)
    return img_path


class _CustomWildsDataset(Dataset):

    def __init__(
            self, wilds_dataset, split_name, domain_label_name, transform, shuffle=False):
        # Get index corresponding to domain label in metadata array
        self.split_name = split_name
        self.dataset_name = wilds_dataset._dataset_name
        if isinstance(domain_label_name, str):
            self.metadata_domain_index = wilds_dataset.metadata_fields.index(
                domain_label_name)
        elif isinstance(domain_label_name, list):
            self.metadata_domain_index = [wilds_dataset.metadata_fields.index(
                d_lab) for d_lab in domain_label_name]

        # Get examples corresponding to split name
        split_examples = wilds_dataset.split_array == wilds_dataset.split_dict[split_name]
        self.data_dir = wilds_dataset.data_dir
        if self.dataset_name == 'fmow':
            self.img_files = wilds_dataset.full_idxs[split_examples]
        elif self.dataset_name in ['camelyon17', 'rxrx1', 'iwildcam']:
            self.img_files = np.array(wilds_dataset._input_array)[
                split_examples]
        self.metadata = wilds_dataset.metadata_array[split_examples]
        self.labels = wilds_dataset.y_array[split_examples]

        if isinstance(self.metadata_domain_index, int):
            self.wilds_domain_labels = self.metadata[
                ..., self.metadata_domain_index].numpy()
        elif isinstance(self.metadata_domain_index, list):
            self.wilds_domain_labels = [
                self.metadata[..., index].numpy() for index in self.metadata_domain_index]
            self.wilds_domain_labels = np.stack(
                self.wilds_domain_labels, axis=-1)

        self.unique_wilds_domain_labels = np.unique(
            self.wilds_domain_labels, axis=0)
        self.num_domains = len(self.unique_wilds_domain_labels)

        self.domain_labels = np.zeros(
            self.wilds_domain_labels.shape[0]).astype(int)
        for i, d in enumerate(self.unique_wilds_domain_labels):
            domain_examples = self.wilds_domain_labels == d
            if domain_examples.ndim == 2:
                domain_examples = domain_examples.all(axis=-1)
            self.domain_labels[domain_examples] = i

        # Get mapping domain --> img_files
        self.domain2files = {d: self.img_files[self.domain_labels == d].tolist() for d
                             in np.unique(self.domain_labels)}
        self.transform = transform
        self.n_classes = wilds_dataset.n_classes

        if shuffle:
            indexes = np.arange(len(self.img_files))
            np.random.shuffle(indexes)
            self.img_files = self.img_files[indexes]
            self.domain_labels = self.domain_labels[indexes]
            self.labels = self.labels[indexes]
            self.metadata = self.metadata[indexes]

    def __len__(self):
        pass

    def __getitem__(self, index):
        pass

    def __repr__(self):
        return f'split_name: {self.split_name}, examples: {len(self.img_files)}, num_domains: {self.num_domains}, num_classes: {self.n_classes}'


class ClassificationWildsDataset(_CustomWildsDataset):

    def __init__(
            self, wilds_dataset, split_name, domain_label_name, transform, shuffle=False):
        super().__init__(
            wilds_dataset, split_name, domain_label_name, transform, shuffle)

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, idx):
        file = self.img_files[idx]
        img_file = get_path(
            dataset_name=self.dataset_name,
            data_dir=self.data_dir,
            filename=file)
        domain = self.domain_labels[idx]
        y = self.labels[idx]
        mdata = self.metadata[idx]

        pil_img = Image.open(img_file).convert("RGB")
        img = self.transform(pil_img)
        pil_img.close()
        return img, domain, y, mdata


class SingleStreamWildsDataset(_CustomWildsDataset):

    def __init__(
            self, wilds_dataset, split_name, domain_label_name, transform, shuffle=False):
        super().__init__(
            wilds_dataset, split_name, domain_label_name, transform, shuffle)

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, idx):
        file = self.img_files[idx]
        img_file = get_path(
            dataset_name=self.dataset_name,
            data_dir=self.data_dir,
            filename=file)
        domain = self.domain_labels[idx]
        pil_img = Image.open(img_file).convert("RGB")
        img = self.transform(pil_img)
        return img, domain


class TwoStreamWildsDataset(_CustomWildsDataset):

    def __init__(
            self, wilds_dataset, split_name, domain_label_name, transform, shuffle=False):
        super().__init__(
            wilds_dataset, split_name, domain_label_name, transform, shuffle)

    def __len__(self):
        return len(self.img_files)

    def __getitem__(self, idx):
        file_1 = self.img_files[idx]
        img_file_1 = get_path(
            dataset_name=self.dataset_name,
            data_dir=self.data_dir,
            filename=file_1)
        domain = self.domain_labels[idx]
        file_2 = random.choice(self.domain2files[domain])
        img_file_2 = get_path(
            dataset_name=self.dataset_name,
            data_dir=self.data_dir,
            filename=file_2)

        pil_img_1 = Image.open(img_file_1).convert("RGB")
        pil_img_2 = Image.open(img_file_2).convert("RGB")
        img_1 = self.transform(pil_img_1)
        img_2 = self.transform(pil_img_2)
        return [img_1, img_2], domain
