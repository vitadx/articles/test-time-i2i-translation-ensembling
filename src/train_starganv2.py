import os
import json
import pprint
import argparse

import torch
from torch import nn
from torchvision import transforms
from torch.optim import Adam
from torch.optim.lr_scheduler import LambdaLR
from torch.utils.data.distributed import DistributedSampler
from wilds import get_dataset

from datasets import SingleStreamWildsDataset, TwoStreamWildsDataset
from dataloaders import InfiniteDataLoader
from models.starganv2 import (
    StyleEncoder, MappingNetwork, Generator, Discriminator)
from models.ema import EMA
from trainers.starganv2 import Trainer
from trainers.diff_augment import DiffAugment
from utils.augmentations import RandomRotation90


def main(
        data_root_dir,
        json_config,
        log_dir,
        checkpoint_dir):

    # Get environment variables for distributed training
    rank = int(os.environ['RANK'])
    local_rank = int(os.environ['LOCAL_RANK'])
    world_size = int(os.environ['WORLD_SIZE'])

    print('ENV VARIABLES'.center(50, '-'))
    print(f'rank: {rank}')
    print(f'local_rank: {local_rank}')
    print(f'world_size: {world_size}')
    print('-'*50+'\n')

    # Load json config file
    with open(json_config, "r") as f:
        hyperparameters = json.load(f)

    # Create log path and checkpoint from exp_name
    hyperparameters.update({
        "ROOT_DIR": data_root_dir,
        "LOG_DIR": os.path.join(log_dir, hyperparameters["DATASET_NAME"],
            hyperparameters["EXP_NAME"]),
        "CHECKPOINT_PATH": os.path.join(
            checkpoint_dir, f'{hyperparameters["EXP_NAME"]}.pth')
    })

    print("Training on: {}".format(hyperparameters["DATASET_NAME"]))
    print(f"Exp_name: {hyperparameters['EXP_NAME']}")

    print('start init_process_group')
    torch.distributed.init_process_group(
        backend="nccl",
        init_method='env://',)
    print('end init_process_group')

    torch.cuda.set_device(local_rank)
    DEVICE = torch.device("cuda")

    if rank == 0:
        pprint.pprint(hyperparameters)

    train_transform = transforms.Compose([
        transforms.RandomResizedCrop(
            hyperparameters["RESIZED_SHAPE"], scale=(0.75, 1), ratio=(0.8, 1.2),
            interpolation=transforms.InterpolationMode.BILINEAR),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.RandomVerticalFlip(p=0.5),
        RandomRotation90(),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=(0.5, 0.5, 0.5),
            std=(0.5, 0.5, 0.5))])

    # Get train, val, test datasets and dataloaders
    torch.distributed.barrier()
    print(f"Loading {hyperparameters['DATASET_NAME']} located in: {data_root_dir}")
    wilds_dataset = get_dataset(
        dataset=hyperparameters["DATASET_NAME"],
        root_dir=data_root_dir,
        download=True)
    datasets = {
        "train": {
            "ref": TwoStreamWildsDataset(
                wilds_dataset=wilds_dataset,
                split_name="train",
                domain_label_name=hyperparameters["DOMAIN_LABEL_NAME"],
                transform=train_transform,
                shuffle=True),
            "src": SingleStreamWildsDataset(
                wilds_dataset=wilds_dataset,
                split_name="train",
                domain_label_name=hyperparameters["DOMAIN_LABEL_NAME"],
                transform=train_transform,
                shuffle=True)},
    }
    hyperparameters["NUM_DOMAINS"] = datasets["train"]["ref"].num_domains
    print(f"The number of domains is: {hyperparameters['NUM_DOMAINS']}")

    dataloaders = {}
    for split_name in datasets:
        if split_name not in dataloaders:
            dataloaders[split_name] = {}
        for dataset_name in datasets[split_name]:
            dataset = datasets[split_name][dataset_name]
            sampler = DistributedSampler(
                dataset=dataset,
                shuffle=True,
                drop_last=True)
            dataloaders[split_name][dataset_name] = InfiniteDataLoader(
                dataset=dataset,
                batch_size=hyperparameters["BATCH_SIZE"],
                num_workers=hyperparameters["NUM_WORKERS"],
                sampler=sampler)

    # Create the different networks of StarGanV2
    style_encoder = StyleEncoder(
        img_size=hyperparameters["IMG_SIZE"],
        style_dim=hyperparameters["STYLE_DIM"],
        num_domains=hyperparameters["NUM_DOMAINS"]).to(DEVICE)
    mapping_network = MappingNetwork(
        latent_dim=hyperparameters["LATENT_DIM"],
        style_dim=hyperparameters["STYLE_DIM"],
        num_domains=hyperparameters["NUM_DOMAINS"],
        hidden_dim=512).to(DEVICE)
    generator = Generator(
        img_size=hyperparameters["IMG_SIZE"],
        style_dim=hyperparameters["STYLE_DIM"],
        use_sn=False).to(DEVICE)
    discriminator = Discriminator(
        img_size=hyperparameters["IMG_SIZE"],
        num_domains=hyperparameters["NUM_DOMAINS"],
        max_conv_dim=512,
        use_sn=False).to(DEVICE)

    # Create the exponential moving average models
    style_encoder_ema = EMA(
        style_encoder, decay=hyperparameters["EMA_DECAY"]).to(DEVICE).eval()
    mapping_network_ema = EMA(
        mapping_network, decay=hyperparameters["EMA_DECAY"]).to(DEVICE).eval()
    generator_ema = EMA(
        generator, decay=hyperparameters["EMA_DECAY"]).to(DEVICE).eval()

    # Create distributed data parallel model
    style_encoder = nn.parallel.DistributedDataParallel(
        style_encoder, device_ids=[local_rank], output_device=local_rank)
    mapping_network = nn.parallel.DistributedDataParallel(
        mapping_network, device_ids=[local_rank], output_device=local_rank)
    generator = nn.parallel.DistributedDataParallel(
        generator, device_ids=[local_rank], output_device=local_rank)
    discriminator = nn.parallel.DistributedDataParallel(
        discriminator, device_ids=[local_rank], output_device=local_rank)

    networks = {
        "S": style_encoder,
        "M": mapping_network,
        "G": generator,
        "D": discriminator,
        "S_EMA": style_encoder_ema,
        "G_EMA": generator_ema,
        "M_EMA": mapping_network_ema,
    }

    # Display number of weights per model:
    print('Number of trainable parameters:')
    for network_name, network in networks.items():
        num_params = sum((p.numel() for p in network.parameters() if p.requires_grad))
        print('{}: {:,}'.format(network_name, num_params))

    optimizers = {
        "SMG": Adam(
            lr=hyperparameters["GENERATOR_LR"],
            params=list(style_encoder.parameters()) + list(generator.parameters()) +\
            list(mapping_network.parameters())),
        "D": Adam(lr=hyperparameters["DISCRIMINATOR_LR"], params=discriminator.parameters()),
    }

    schedulers = {
        "SMG": LambdaLR(optimizers["SMG"], lr_lambda=lambda epoch: 1)
    }

    # Create DiffAugment module to performs data augmentation on GPU
    augmentations = {
        "diff_aug": DiffAugment(
            p=1).to(DEVICE)}

    if os.path.exists(hyperparameters["CHECKPOINT_PATH"]):
        print("Loading previous checkpoint")
        map_location = {
            'cuda:%d' % 0: 'cuda:%d' % local_rank}
        chkpt = torch.load(
            hyperparameters["CHECKPOINT_PATH"],
            map_location=map_location)
        for net_name, state_dict in chkpt["networks"].items():
            networks[net_name].load_state_dict(
                state_dict)
        for opt_name, state_dict in chkpt["optimizers"].items():
            optimizers[opt_name].load_state_dict(
                state_dict)
        for sched_name, state_dict in chkpt["schedulers"].items():
            schedulers[sched_name].load_state_dict(
                state_dict)
        if "augmentations" in chkpt:
            for aug_name, state_dict in chkpt["augmentations"].items():
                augmentations[aug_name].load_state_dict(
                    state_dict)

    print("Start training")
    trainer = Trainer()
    trainer.train(
        networks=networks,
        dataloaders=dataloaders,
        optimizers=optimizers,
        schedulers=schedulers,
        augmentations=augmentations,
        epochs=hyperparameters["EPOCHS"],
        steps_per_epoch=hyperparameters["STEPS_PER_EPOCH"],
        hyperparameters=hyperparameters,
        device=DEVICE,
        log_dir=hyperparameters["LOG_DIR"],
        checkpoint_path=hyperparameters["CHECKPOINT_PATH"],
        save_every_epochs=hyperparameters["SAVE_EVERY_EPOCHS"],
        rank=rank)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--data_root_dir",
        help="Path to data root directory.", type=str)
    parser.add_argument(
        "--json_config",
        help="Path to json config file for StarGANV2 training.", type=str)
    parser.add_argument(
        "--log_dir",
        help="Logs directory",
        type=str)
    parser.add_argument(
        "--checkpoint_dir",
        help="Checkpoints directory",
        type=str)
    args = parser.parse_args()
    main(
        data_root_dir=args.data_root_dir,
        json_config=args.json_config,
        log_dir=args.log_dir,
        checkpoint_dir=args.checkpoint_dir,)
