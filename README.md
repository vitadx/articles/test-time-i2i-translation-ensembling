# Test-time image-to-image translation ensembling improves out-of-distribution generalization in histopathology

<center>
	<h2 style="text-align: center;">| <a href="https://arxiv.org/pdf/2206.09769.pdf">Paper</a> |</h2>
</center>

This repository contains the official pytorch implementation of the test-time augmentation method presented in the paper [Test-time image-to-image translation ensembling improves out-of-distribution generalization in histopathology](https://arxiv.org/pdf/2206.09769.pdf).

<figure>
<img src="./imgs/i2i_ensembling.png" width="100%" align="center" alt="Image-to-image translation ensembling diagram">
 <figcaption style='text-align: center;'><b>Test-time data augmentation with latent-guided StarGanV2 image-to-image translation</b></figcaption>
</figure>


Given a test image $`x`$, we sample $`S`$ random latent code vectors $`\{z_1, ..., z_S\} \sim \mathcal{N}(0, I)`$ with respective domain label $`\{d_1, ..., d_S\}`$. Each latent code vector along with its domain label are fed to the mapping network $`F`$ to produce the different style vectors $`\{s_1, ..., s_S\} =\left \{F(z_1, d_1), ..., F(z_S, d_S) \right\}`$. Then, the image $`x`$, along with each of the style vectors $`\{s_1, ..., s_S\}`$, is fed to the StarGanV2 generator $`G`$ translating $`x`$ into the images $`\{x_1, ..., x_S\} = \left \{G(x, s_1), ..., G(x, s_S) \right\}`$. From each $`x_i`$ that should have now similar characteristics as real images from domain $`d_i`$, we compute the predictions of a shared classifier $`\hat{y}_i = C(x_i)`$ and the different StarGanV2 domain discriminators score $`\hat{d}_i = D_i(x_i)`$ reflecting how well the image $`x`$ has been projected into domain $`d_i`$. Finally, from the classifier predictions $`\{\hat{y}_1, ..., \hat{y}_S\}`$ and the domain discriminators score $`\{\hat{d}_1, ..., \hat{d}_S\}`$, we propose 3 different strategies to ensemble the predictions into a final prediction $`\hat{y}`$.

## :hammer: Requirements

### Setup environment via Conda

If you are using conda to manage your python environments, you need first to create a conda environment and install the different dependencies specified in `i2i_tta_env.yml` via the following command:
```bash
conda create -n i2i_tta --file i2i_tta_env.yml
```
and then activate the environment:
```bash
conda activate i2i_tta
```


## :computer: Run experiments

### StarGanV2 training

```bash
torchrun --nnodes=1 --nproc_per_node=1 train_starganv2.py \
--data_root_dir=./../data \
--json_config=./configs/starganv2_camelyon17.json \
--log_dir=./../logs \
--checkpoint_dir=./../checkpoints
```
If the camelyon17 WILDS dataset is not already in the specified `data_root_dir` data will be downloaded. To modify hyperparameters please refer to the json config [file](src/configs/starganv2_camelyon17.json)

### Classifier training and evaluation

```bash
python train_eval_classifier.py \
--data_root_dir=./../data \
--i2i_chkpt_path=./../checkpoints/starganv2_camelyon17.pth \
--json_config=./configs/densenet121.json \
--log_dir=./../logs \
--checkpoint_dir=./../checkpoints \
--result_dir=./../results \
--exp_index=1
````

### Results

We report the accuracy with this official code on a single run. `base` method, as in the paper, corresponds to a model trained with cross-entropy.

|         method                 |   id_val   |   val   |   test   |
|:-------------------------------|:-----------|:--------|:---------|
|         base w/o TTA           |    98.10   |  83.55  |   74.84  |
|         base w TTA             |    98.55   |  92.10  |   93.14  |
| base + consistency reg w/o TTA |    97.97   |  87.34  |   78.17  |
| base + consistency reg w/ TTA  |    98.47   |  92.11  |   93.30  |


## :pencil2: Citation

If you use our work for further research, please cite us:

```
@inproceedings{scalbert2022test,
  title={Test-time image-to-image translation ensembling improves out-of-distribution generalization in histopathology},
  author={Scalbert, Marin and Vakalopoulou, Maria and Couzini{\'e}-Devy, Florent},
  booktitle={International Conference on Medical Image Computing and Computer-Assisted Intervention},
  pages={120--129},
  year={2022},
  organization={Springer}
}
```

## :envelope: Contact us

If you have any technical issues with the code or questions regarding the paper, you can contact us via this [mail adress](mailto:marin.scalbert@centralesupelec.fr).
